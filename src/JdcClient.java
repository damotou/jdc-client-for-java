
import java.awt.Color;
import java.awt.Dialog;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.TrayIcon.MessageType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import jpcap.JpcapCaptor;
import jpcap.JpcapSender;
import jpcap.NetworkInterface;
import jpcap.packet.EthernetPacket;
import jpcap.packet.Packet;
import packet.EAPOLPacket;
import packet.EAPPacket;


public class JdcClient {

	private JFrame frame;
	private JTextField Tname;
	private JPasswordField passwordField;
	private boolean selectrepass;
	private int select;
	/**
	 * Launch the application.
	 */
	/** EAP packet frame type */
	public static final short ETHERTYPE_EAP = (short) 0x888e;
	
	/** Broadcast address of EAP packet */
	public static final byte[] BROADCAST_ADDRESS = {(byte)0x01,(byte)0x80,(byte)0xc2,(byte)0x00,(byte)0x00,(byte)0x03};
	
	/** EAP packet header's <EM>TYPE</EM> as a identity */
	public static final byte TYPE_OF_IDENTITY = 0x01;
	
	/** EAP packet header's <EM>TYPE</EM> as a md5-challenge */
	public static final byte TYPE_OF_MD5_CHALLENGE = 0x04;
	
	/** Indicating as a identity request packet */
	public static final byte[] IDENTITY_REQUEST = {
		EAPOLPacket.VERSION_1, EAPOLPacket.EAP_PACKET, 0x00, 0x05, 
		EAPPacket.REQUEST, EAPPacket.IDENTITY, 0x00, 0x05, TYPE_OF_IDENTITY
	};
	
	/** Indicating as a md5-challenge request packet */
	public static final byte[] MD5_CHALLENGE_REQUEST = {
		EAPOLPacket.VERSION_1, EAPOLPacket.EAP_PACKET, 0x00, 0x16, 
		EAPPacket.REQUEST, EAPPacket.MD5_CHALLENGE, 0x00, 0x16, TYPE_OF_MD5_CHALLENGE
	};
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JdcClient window = new JdcClient();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public JdcClient() {
		//System.out.println(System.getProperty("java.library.path"));  
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBackground(Color.LIGHT_GRAY);
		frame.setTitle("\u795E\u5DDE\u6570\u7801\u8BA4\u8BC1\u5BA2\u6237\u7AEF");
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage("E:\\\u751F\u6D3B\\qzone\\\u559C\u6B22\\444.jpg"));
		frame.setBounds(100, 100, 379, 297);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel Tusername = new JLabel("\u7528\u6237\u540D:");
		Tusername.setBounds(51, 49, 54, 28);
		frame.getContentPane().add(Tusername);
		
		Tname = new JTextField();
		//Tname.setText("20094042070");
		Tname.setBounds(109, 53, 193, 21);
		frame.getContentPane().add(Tname);
		Tname.setColumns(10);
		
		JLabel Tpass = new JLabel("\u5BC6  \u7801:");
		Tpass.setBounds(51, 87, 54, 28);
		frame.getContentPane().add(Tpass);
		
		JCheckBox checkBox = new JCheckBox("\u8BB0\u4F4F\u5BC6\u7801");
		checkBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (selectrepass) {
					selectrepass=false;
				}else{
					selectrepass=true;
				}
				//System.out.println(selectrepass);
				//selectrepass =true
			}
		});
		checkBox.setBounds(109, 163, 91, 23);
		frame.getContentPane().add(checkBox);
		
		JButton button = new JButton("\u767B\u9646");
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				
				String username =Tname.getText().trim().toString();
				String password =passwordField.getText().trim().toString();
				EthernetPacket ether = new EthernetPacket();
				ether.frametype = ETHERTYPE_EAP;
				 NetworkInterface[] devices = JpcapCaptor.getDeviceList();
				ether.src_mac = devices[select].mac_address;
				ether.dst_mac = BROADCAST_ADDRESS;
				/* start packet */
				EAPOLPacket startPacket = new EAPOLPacket();
				startPacket.setEAPOLParameter(EAPOLPacket.VERSION_1, EAPOLPacket.EAPOL_START);
				startPacket.datalink = ether;
				/* logoff packet */
				EAPOLPacket logoffPacket = new EAPOLPacket();
				logoffPacket.setEAPOLParameter(EAPOLPacket.VERSION_1, EAPOLPacket.EAPOL_LOGOFF);
				logoffPacket.datalink = ether;
				/* identity packet */
				EAPPacket identityPacket = new EAPPacket(EAPPacket.RESPONSE, EAPPacket.IDENTITY, EAPPacket.IDENTITY_TYPE, username, EAPOLPacket.VERSION_1, EAPOLPacket.EAP_PACKET, devices[select].addresses[0].address.getAddress());
				identityPacket.datalink = ether;
				/* md5-challenge packet */
				EAPPacket md5challengePacket = new EAPPacket(EAPPacket.RESPONSE, EAPPacket.MD5_CHALLENGE, EAPPacket.MD5_CHALLENGE_TYPE, username, EAPOLPacket.VERSION_1, EAPOLPacket.EAP_PACKET, devices[select].addresses[0].address.getAddress());
				md5challengePacket.datalink = ether;
				/* handshake packet */
				EAPPacket handshakePacket = new EAPPacket(EAPPacket.RESPONSE, (byte)0x03, EAPPacket.HANDSHAKE_TYPE, username, EAPOLPacket.VERSION_1, EAPOLPacket.EAP_PACKET, devices[select].addresses[0].address.getAddress());
				handshakePacket.datalink = ether;
				/* authenticate */
				JpcapCaptor captor;
				try {
					captor = JpcapCaptor.openDevice(devices[select], 65535, false, 20);
					captor.setFilter("ether proto 0x888e", false);
					JpcapSender sender = JpcapSender.openDevice(devices[select]);
					sender.sendPacket(logoffPacket);
					sender.sendPacket(startPacket);
					byte[] eapHeader = new byte[9];
					byte[] keys = new byte[16];
					byte[] handshakeKeys = new byte[4];
					long count = 0;
					while(true) {
						Packet receivedPacket = captor.getPacket();
						if(receivedPacket == null) continue;
						for(int i = 0; i < 9; i++) {
							eapHeader[i] = receivedPacket.data[i];
						}
						if(Arrays.equals(eapHeader, IDENTITY_REQUEST)) {
							sender.sendPacket(identityPacket);
							//JOptionPane.showConfirmDialog(frame, "正在认证.....", "神州数码认证客户端", JOptionPane.CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null);
							//System.out.println("Sending username...");
						}
						else if(Arrays.equals(eapHeader, MD5_CHALLENGE_REQUEST)) {
							for(int i = 0; i < 16; i++) {
								keys[i] = receivedPacket.data[i+10];
							}
							md5challengePacket.fillEncryptedPassword(password, keys);
							sender.sendPacket(md5challengePacket);
							//System.out.println("Sending password...");
						}
						else if(receivedPacket.data[4] == EAPPacket.SUCCESS) {
							JOptionPane.showMessageDialog(frame, "认证成功", "神州数码认证客户端",JOptionPane.INFORMATION_MESSAGE);
							if (selectrepass) {
								
								
							}
							//System.out.println("Authentication [SUCCESS]: " + new Date().toString());
							
							frame.setVisible(false);
						}
						else if(receivedPacket.data[4] == EAPPacket.FAILURE) {
							JOptionPane.showMessageDialog(frame, "认证失败", "神州数码认证客户端",JOptionPane.INFORMATION_MESSAGE);
							System.out.println("Authentication [FAILURE]: " + new Date().toString());
							frame.setVisible(true);
							break;
						}
						else if(receivedPacket.data[4] == EAPPacket.REQUEST && receivedPacket.data[8] == EAPPacket.HANDSHAKE_TYPE) {
							for(int i = 0; i < 4; i++) {
								handshakeKeys[i] = receivedPacket.data[i+9];
							}
							handshakePacket.fillEncryptedHandshake(receivedPacket.data[5], username, handshakeKeys);
							sender.sendPacket(handshakePacket);
							//System.out.println("I am still alive, count=" + count++);
						}
					}
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
			}
		});
		button.setBounds(230, 163, 72, 23);
		frame.getContentPane().add(button);
		
		passwordField = new JPasswordField();
		//passwordField.setText("19901003");
		passwordField.setBounds(109, 91, 193, 21);
		frame.getContentPane().add(passwordField);
		
		final JComboBox comboBox = new JComboBox();
		NetworkInterface[] devices = JpcapCaptor.getDeviceList();
		String [] device=new String[devices.length] ;
		for(int i = 0; i < devices.length; i++) {
			device[i]=devices[i].description ;
		}
		comboBox.setModel(new DefaultComboBoxModel(device));
		comboBox.setBounds(109, 122, 193, 21);
		comboBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				select=comboBox.getSelectedIndex();
				
			}
		} );
		frame.getContentPane().add(comboBox);
		
		JLabel label = new JLabel("\u9009\u62E9\u7F51\u5361");
		label.setBounds(51, 125, 54, 15);
		frame.getContentPane().add(label);
		
		
	}
}
